package com.example.appmegar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleUniformesFragment extends Fragment {

    TextView nom, prec, desc;
    ImageView img;


    public DetalleUniformesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalle_uniformes, container, false);

        nom = (TextView)view.findViewById(R.id.det_txtNom_uniformes);
        prec = (TextView)view.findViewById(R.id.det_txtPre_uniformes);
        desc = (TextView)view.findViewById(R.id.det_txtDesc_uniformes);
        img = (ImageView)view.findViewById(R.id.det_img_uniformes);

        Bundle uniformes = getArguments();
        ProductosModel datosUniformes = null;

        if(uniformes != null){
            datosUniformes = (ProductosModel) uniformes.getSerializable("uniformes");

            Picasso.get().load(datosUniformes.getImage()).into(img);
            nom.setText(datosUniformes.getName());
            prec.setText(datosUniformes.getPrec());
            desc.setText(datosUniformes.getDesclarg());

        }


        return view;
    }

}
