package com.example.appmegar.ui;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.appmegar.R;
import com.example.appmegar.ui.FiveFragment;
import com.example.appmegar.ui.FourFragment;
import com.example.appmegar.ui.OneFragment;
import com.example.appmegar.ui.ThreeFragment;
import com.example.appmegar.ui.TwoFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment {


    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_welcome, container, false);

        MyPagerAdapter myPagerAdapter =
                new MyPagerAdapter(getFragmentManager());
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(myPagerAdapter);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(viewPager);



        return view;
    }

    class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = new OneFragment();
                    break;
                case 1:
                    fragment = new TwoFragment();
                    break;
                case 2:
                    fragment = new ThreeFragment();
                    break;
                case 3:
                    fragment = new FourFragment();
                    break;
                case 4:
                    fragment = new FiveFragment();
                    break;
                default:
                    fragment = null;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Inicio";
                case 1:
                    return "Empaque";
                case 2:
                    return "Seguridad";
                case 3:
                    return "Limpieza";
                case 4:
                    return "Uniformes";
            }
            return null;
        }


    }

}
