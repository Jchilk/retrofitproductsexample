package com.example.appmegar.ui;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appmegar.MainActivity;
import com.example.appmegar.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment {

    //Metodo para pasar usuario a texto y Logout
    private TextView textView;
    private Button LogOutbutton;
    OnLogoutListener logoutListener;


    public interface OnLogoutListener{

        public void logoutPerformed();
    }

    public OneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_one, container, false);

        textView = (TextView) view.findViewById(R.id.text_name_info);
        textView.setText("Welcome " + MainActivity.prefConfig.readName());
        LogOutbutton = (Button) view.findViewById(R.id.bn_logout);

        //Metodo para pasar usuario a texto y Logout
        LogOutbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutListener.logoutPerformed();
            }
        });


        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //Metodo para pasar usuario a texto y Logout
        Activity activity = (Activity) context;
        logoutListener = (OnLogoutListener)activity;

    }
}
