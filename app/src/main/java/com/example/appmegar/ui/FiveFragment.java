package com.example.appmegar.ui;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appmegar.IComunicationFragments;
import com.example.appmegar.MainActivity;
import com.example.appmegar.ProductosModel;
import com.example.appmegar.R;
import com.example.appmegar.RequestInterface;
import com.example.appmegar.adapters.UniformesAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class FiveFragment extends Fragment {

    private TextView textView;
    private Button LogOutbutton;
    OneFragment.OnLogoutListener logoutListener;

    ArrayList<ProductosModel> productsModels = new ArrayList<>();
    private UniformesAdapter uniformesAdapter;
    private RecyclerView uniformes_recyclerview;

    //Metodo para pasar a detalle
    Activity activity;
    IComunicationFragments iComunicationFragments;


    public interface OnLogoutListener{

        public void logoutPerformed();
    }


    public FiveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_five, container, false);


        textView = (TextView) view.findViewById(R.id.text_name_info_5);
        textView.setText("Welcome " + MainActivity.prefConfig.readName());
        LogOutbutton = (Button) view.findViewById(R.id.bn_logout_5);


        uniformes_recyclerview = (RecyclerView) view.findViewById(R.id.recyclerUniformes);
        uniformes_recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));

        //Metodo para pasar usuario a texto y Logout
        LogOutbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutListener.logoutPerformed();
            }
        });

        getProductResponse();

        return view;
    }

    private void getProductResponse(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.87/productos/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final RequestInterface requestInterface = retrofit.create(RequestInterface.class);
        Call<List<ProductosModel>> call = requestInterface.getUniformesJson();

        call.enqueue(new Callback<List<ProductosModel>>() {
            @Override
            public void onResponse(Call<List<ProductosModel>> call, Response<List<ProductosModel>> response) {
                productsModels = new ArrayList<>(response.body());
                uniformesAdapter = new UniformesAdapter(productsModels,getContext());
                uniformes_recyclerview.setAdapter(uniformesAdapter);


                MainActivity.prefConfig.displayMToast("Succes");

                uniformesAdapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        MainActivity.prefConfig.displayMToast("Yup...");

                        iComunicationFragments.datosUniformes
                                (productsModels.get(uniformes_recyclerview.getChildAdapterPosition(v)));
                    }
                });


            }

            @Override
            public void onFailure(Call<List<ProductosModel>> call, Throwable t) {
                Toast.makeText(getContext(),"Failed" + t.toString(),Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //Metodo para pasar usuario a texto y Logout
        Activity activity = (Activity) context;
        logoutListener = (OneFragment.OnLogoutListener)activity;

        //Metodo para pasar a detalle
        if (context instanceof Activity) {
            this.activity = (Activity) context;
            iComunicationFragments = (IComunicationFragments) this.activity;
        }

    }

}
