package com.example.appmegar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleEmpaqueFragment extends Fragment {

    TextView nom, prec, desc;
    ImageView img;

    ProductosModel productosModel;


    public DetalleEmpaqueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_detalle_empaque, container, false);

        nom = (TextView)view.findViewById(R.id.det_txtNom_empaque);
        prec = (TextView)view.findViewById(R.id.det_txtPre_empaque);
        desc = (TextView)view.findViewById(R.id.det_txtDesc_empaque);
        img = (ImageView)view.findViewById(R.id.det_img_empaque);

        Bundle empaque = getArguments();
        ProductosModel datosEmpaque = null;

        if(empaque != null){
            datosEmpaque = (ProductosModel) empaque.getSerializable("empaque");

            Picasso.get().load(datosEmpaque.getImage()).into(img);
            nom.setText(datosEmpaque.getName());
            prec.setText(datosEmpaque.getPrec());
            desc.setText(datosEmpaque.getDesclarg());

        }

        return view;
    }

}
