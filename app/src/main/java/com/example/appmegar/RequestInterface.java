package com.example.appmegar;



import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestInterface {

    @GET("products.php")
    Call<List<ProductosModel>> getProductsEmpaqueJson();

    @GET("seguridad.php")
    Call<List<ProductosModel>> getSeguridadJson();

    @GET("limpieza.php")
    Call<List<ProductosModel>> getLimpiezaJson();

    @GET("uniformes.php")
    Call<List<ProductosModel>> getUniformesJson();


}
