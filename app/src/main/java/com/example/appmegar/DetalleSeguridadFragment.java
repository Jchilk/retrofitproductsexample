package com.example.appmegar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleSeguridadFragment extends Fragment {

    TextView nom, prec, desc;
    ImageView img;


    public DetalleSeguridadFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalle_seguridad, container, false);


        nom = (TextView)view.findViewById(R.id.det_txtNom_seguridad);
        prec = (TextView)view.findViewById(R.id.det_txtPre_seguridad);
        desc = (TextView)view.findViewById(R.id.det_txtDesc_seguridad);
        img = (ImageView)view.findViewById(R.id.det_img_seguridad);

        Bundle seguridad = getArguments();
        ProductosModel datosSeguridad = null;

        if(seguridad != null){
            datosSeguridad = (ProductosModel) seguridad.getSerializable("seguridad");

            Picasso.get().load(datosSeguridad.getImage()).into(img);
            nom.setText(datosSeguridad.getName());
            prec.setText(datosSeguridad.getPrec());
            desc.setText(datosSeguridad.getDesclarg());

        }
        return view;
    }

}
