package com.example.appmegar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment {

    private EditText Name,UserName,UserPassword;
    private Button BnRegister;
    private TextView textViewResult;


    public RegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        Name = (EditText) view.findViewById(R.id.txt_name);
        UserName = (EditText) view.findViewById(R.id.txt_user_name);
        UserPassword = (EditText) view.findViewById(R.id.txt_password);
        BnRegister = (Button) view.findViewById(R.id.btn_register);
        textViewResult = (TextView) view.findViewById(R.id.text_view_result);

        BnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                perfomRegistration();

            }
        });


        return view;
    }

    public void perfomRegistration(){

        String name = Name.getText().toString();
        String userName = UserName.getText().toString();
        String password = UserPassword.getText().toString();

        Call<User> call = MainActivity.apiInterface.performRegistration(name,userName,password);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if(response.body().getResponse().equals("ok")){

                    MainActivity.prefConfig.displayMToast("Registration success . . . . ");

                }else if(response.body().getResponse().equals("exist")){

                    MainActivity.prefConfig.displayMToast("User already exist . . . . ");

                }else if(response.body().getResponse().equals("error")){

                    MainActivity.prefConfig.displayMToast("Something went wrong. . . .");

                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

        Name.setText("");
        UserName.setText("");
        UserPassword.setText("");

    }

}
