package com.example.appmegar;

public interface IComunicationFragments {

    public void datosEmpaque(ProductosModel productosModel);

    public void datosSeguridad(ProductosModel productosModel);

    public void datosLimpieza(ProductosModel productosModel);

    public void datosUniformes(ProductosModel productosModel);

}
