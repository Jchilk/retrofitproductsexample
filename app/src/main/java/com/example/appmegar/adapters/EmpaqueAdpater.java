package com.example.appmegar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appmegar.ProductosModel;
import com.example.appmegar.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EmpaqueAdpater extends RecyclerView.Adapter<EmpaqueAdpater.ViewHolder> implements
        View.OnClickListener{

    private ArrayList<ProductosModel> productosEmpaque = new ArrayList<>();
    private Context context;
    private View.OnClickListener listener;

    public EmpaqueAdpater(ArrayList<ProductosModel> productosEmpaque, Context context) {
        this.productosEmpaque = productosEmpaque;
        this.context = context;
    }

    @NonNull
    @Override
    public EmpaqueAdpater.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list,viewGroup,false);
        view.setOnClickListener(this);

        return new EmpaqueAdpater.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmpaqueAdpater.ViewHolder viewHolder, int i) {

        viewHolder.name.setText(productosEmpaque.get(i).getName());
        viewHolder.desc.setText(productosEmpaque.get(i).getDesc());
        viewHolder.prec.setText(productosEmpaque.get(i).getPrec());

        Picasso.get().load(productosEmpaque.get(i).getImage()).into(viewHolder.img);

    }

    @Override
    public int getItemCount() {
        return productosEmpaque.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name,desc,prec;
        private ImageView img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.product_name);
            desc = (TextView) itemView.findViewById(R.id.product_desc);
            prec = (TextView) itemView.findViewById(R.id.product_prec);
            img = (ImageView) itemView.findViewById(R.id.product_image);

        }
    }
}
