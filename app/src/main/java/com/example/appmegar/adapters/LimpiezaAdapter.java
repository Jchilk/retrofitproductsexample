package com.example.appmegar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appmegar.ProductosModel;
import com.example.appmegar.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class LimpiezaAdapter extends RecyclerView.Adapter<LimpiezaAdapter.ViewHolder>
        implements View.OnClickListener {

    private ArrayList<ProductosModel> productosLimpieza = new ArrayList<>();
    private Context context;
    private View.OnClickListener listener;

    public LimpiezaAdapter(ArrayList<ProductosModel> productosLimpieza, Context context) {
        this.productosLimpieza = productosLimpieza;
        this.context = context;
    }

    @NonNull
    @Override
    public LimpiezaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list,viewGroup,false);
        view.setOnClickListener(this);
        return new LimpiezaAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LimpiezaAdapter.ViewHolder viewHolder, int i) {

        viewHolder.name.setText(productosLimpieza.get(i).getName());
        viewHolder.desc.setText(productosLimpieza.get(i).getDesc());
        viewHolder.prec.setText(productosLimpieza.get(i).getPrec());

        Picasso.get().load(productosLimpieza.get(i).getImage()).into(viewHolder.img);


    }

    @Override
    public int getItemCount() {
        return productosLimpieza.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView name,desc,prec;
        private ImageView img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.product_name);
            desc = (TextView) itemView.findViewById(R.id.product_desc);
            prec = (TextView) itemView.findViewById(R.id.product_prec);
            img = (ImageView) itemView.findViewById(R.id.product_image);

        }
    }
}
