package com.example.appmegar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleLimpiezaFragment extends Fragment {

    TextView nom, prec, desc;
    ImageView img;

    public DetalleLimpiezaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalle_limpieza, container, false);

        nom = (TextView)view.findViewById(R.id.det_txtNom_limpieza);
        prec = (TextView)view.findViewById(R.id.det_txtPre_limpieza);
        desc = (TextView)view.findViewById(R.id.det_txtDesc_limpieza);
        img = (ImageView)view.findViewById(R.id.det_img_limpieza);

        Bundle limpieza = getArguments();
        ProductosModel datosLimpieza = null;

        if(limpieza != null){
            datosLimpieza = (ProductosModel) limpieza.getSerializable("limpieza");

            Picasso.get().load(datosLimpieza.getImage()).into(img);
            nom.setText(datosLimpieza.getName());
            prec.setText(datosLimpieza.getPrec());
            desc.setText(datosLimpieza.getDesclarg());

        }



        return view;
    }

}
