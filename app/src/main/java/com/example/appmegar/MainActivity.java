package com.example.appmegar;

import android.app.SearchManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;

import com.example.appmegar.ui.FiveFragment;
import com.example.appmegar.ui.FourFragment;
import com.example.appmegar.ui.OneFragment;
import com.example.appmegar.ui.ThreeFragment;
import com.example.appmegar.ui.TwoFragment;
import com.example.appmegar.ui.WelcomeFragment;

public class MainActivity extends AppCompatActivity implements LoginFragment.OnLoginFormActivityListener, OneFragment.OnLogoutListener,
        TwoFragment.OnLogoutListener, ThreeFragment.OnLogoutListener, FourFragment.OnLogoutListener,
        FiveFragment.OnLogoutListener, IComunicationFragments{

    public static PrefConfig prefConfig;
    public static ApiInterface apiInterface;

    DetalleEmpaqueFragment detalleEmpaqueFragment;
    DetalleSeguridadFragment detalleSeguridadFragment;
    DetalleLimpiezaFragment detalleLimpiezaFragment;
    DetalleUniformesFragment detalleUniformesFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefConfig = new PrefConfig(this);
        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        if(findViewById(R.id.fragment_conteiner)!= null){

            if(savedInstanceState != null){

                return;
            }

            if(prefConfig.readLoginstatus()){

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,
                        new WelcomeFragment()).commit();

            }
            else {

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,
                        new LoginFragment()).commit();

            }

        }
    }

    @Override
    public void performRegister() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,
                new RegistrationFragment()).addToBackStack(null).commit();
    }

    @Override
    public void performLogin(String name) {
        prefConfig.writeName(name);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,new WelcomeFragment()).commit();
    }

    @Override
    public void logoutPerformed() {
        prefConfig.writeLoginStatus(false);
        prefConfig.writeName("User");
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,new LoginFragment()).commit();

    }

    @Override
    public void datosEmpaque(ProductosModel productosModel) {
        detalleEmpaqueFragment = new DetalleEmpaqueFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("empaque",productosModel);
        detalleEmpaqueFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,detalleEmpaqueFragment).addToBackStack("fragment_conteiner").commit();


    }

    @Override
    public void datosSeguridad(ProductosModel productosModel) {
        detalleSeguridadFragment = new DetalleSeguridadFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("seguridad",productosModel);
        detalleSeguridadFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,detalleSeguridadFragment).addToBackStack("fragment_conteiner").commit();


    }

    @Override
    public void datosLimpieza(ProductosModel productosModel) {
        detalleLimpiezaFragment = new DetalleLimpiezaFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("limpieza",productosModel);
        detalleLimpiezaFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,detalleLimpiezaFragment).addToBackStack("fragment_conteiner").commit();



    }

    @Override
    public void datosUniformes(ProductosModel productosModel) {
        detalleUniformesFragment = new DetalleUniformesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("uniformes",productosModel);
        detalleUniformesFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_conteiner,detalleUniformesFragment).addToBackStack("fragment_conteiner").commit();



    }

}
